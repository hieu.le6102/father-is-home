﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ItemManager : MonoBehaviour
{
    public Transform[] spawnPoint;
    public GameObject[] itemPrefabs;
    public List<GameObject> activeItem = new List<GameObject>();
    public List<ItemController> activeItemController = new List<ItemController>();
    static List<int> randomNumber = new List<int>{1,2,4,5};
    static List<int> randomNumber1 = new List<int>{0,2,4,5};
    static List<int> randomNumber2 = new List<int>{0,1,2,3};
    static List<int> randomNumber3 = new List<int>{2,3,4,5};

    public List<List<int>> listRandomNumber = new List<List<int>>{randomNumber, randomNumber1, randomNumber2, randomNumber3};
    public int CountItem;

    static Random random = new Random();
    void Start()
    {
        foreach (int i in listRandomNumber[Random.Range(0, listRandomNumber.Count)])
        {
            SpawnItem(Random.Range(0, itemPrefabs.Length), i);
        }
    }
    void Update()
    {
        if (CountItem >= 4)
        {
            var parameters = new LoadSceneParameters(LoadSceneMode.Single);
            GameObject gameObject = GameObject.Find("SceneVariables");
            StoreVariable store = gameObject.GetComponent<StoreVariable>();
            store.score += 1;
            SceneManager.LoadScene("Main Board", parameters);   
        }
    }
   
    public void SpawnItem(int itemIndex, int index)
    {
        GameObject item = (GameObject)Instantiate(itemPrefabs[itemIndex], spawnPoint[index].position, spawnPoint[index].rotation);
        ItemController itemController = item.GetComponent<ItemController>();
        item.tag = "Item";
        
        item.transform.position = new Vector3( item.transform.position.x,  item.transform.position.y, 0);
        activeItem.Add(item);
        activeItemController.Add(itemController);
        int temp = 0;
        foreach (GameObject i in activeItem)
        {
            if (i == item)
            {
                itemController.index = temp;
                break;
            }
            else
            {
                temp += 1;
            }
        }
        
        
    }

    public void DeleteItem(int itemIndex)
    {
        Destroy(activeItem[itemIndex]);
        CountItem += 1;
    }

    public void moveRandomTrash()
    {
        int itemIndex = Random.Range(0, activeItem.Count);
        try {
            activeItemController[itemIndex].MoveTrash();
        }
        catch
        {
            
        }
        

    }
    
}
