﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemController : MonoBehaviour
{
    public Rigidbody2D body;
    public Transform item;
    public Transform trash;
    public ItemHud hud;
    
    public int index;
    // Start is called before the first frame update
    void Start()
    {
        body.gravityScale = 0;
        GameObject gameObject = GameObject.Find("ItemHUD");
        hud = gameObject.GetComponent<ItemHud>();
        GameObject gameObject1 = GameObject.Find("Trash");
        trash = gameObject1.GetComponent<Transform>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void MoveTrash()
    {
        Vector2 direction = new Vector2(item.position.x, item.position.y) - new Vector2(trash.position.x, trash.position.y);
        item.Translate(direction * -1 * hud.speedItem * Time.deltaTime);
    }

}
