﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemDelete : MonoBehaviour
{
    // Start is called before the first frame update
    public ItemManager itemManager;

    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        GameObject gameObject = collision.gameObject;
        ItemController itemController = gameObject.GetComponent<ItemController>();
        if (collision.gameObject.tag == "Item")
        {            
            itemManager.DeleteItem(itemController.index);
        }

    }
}
