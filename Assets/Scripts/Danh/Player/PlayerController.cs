﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using System.Linq;


public class PlayerController : MonoBehaviour
{
    public Transform player;
    public GameObject ButtonRequestGarbageCollection;
    public GameObject ButtonRequestDishWashing;
    public GameObject ButtonRequestDragAndDrop;
    public GameObject ButtonRequestHaveApple;

    void Start()
    {
        ButtonRequestGarbageCollection.SetActive(false);
        ButtonRequestDishWashing.SetActive(false);
        ButtonRequestDragAndDrop.SetActive(false);
        ButtonRequestHaveApple.SetActive(false);
        GameObject gameObject = GameObject.Find("SceneVariables");
        StoreVariable store = gameObject.GetComponent<StoreVariable>();
        player.position = new Vector3(store.player_x, store.player_y, 0);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        print(collision.gameObject.name);
        if (collision.gameObject.name == "QuestGarbageCollection(Clone)")
        {
            ButtonRequestGarbageCollection.SetActive(true);
        }
        if (collision.gameObject.name == "QuestDishWashing(Clone)")
        {
            ButtonRequestDishWashing.SetActive(true);
        }
        if (collision.gameObject.name == "QuestDragAndDrop(Clone)")
        {
            ButtonRequestDragAndDrop.SetActive(true);
        }
        if (collision.gameObject.name == "QuestHaveApple(Clone)")
        {
            ButtonRequestHaveApple.SetActive(true);
        }
        // if (collision.gameObject.tag == "Request")
        // {
        //     ButtonRequest.SetActive(true);
        // }
        // if (collision.gameObject.tag == "Request")
        // {
        //     ButtonRequest.SetActive(true);
        // }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.name == "QuestGarbageCollection(Clone)")
        {
            ButtonRequestGarbageCollection.SetActive(false);
        }
        if (collision.gameObject.name == "QuestDishWashing(Clone)")
        {
            ButtonRequestDishWashing.SetActive(false);
        }
        if (collision.gameObject.name == "QuestDragAndDrop(Clone)")
        {
            ButtonRequestDragAndDrop.SetActive(false);
        }
        if (collision.gameObject.name == "QuestHaveApple(Clone)")
        {
            ButtonRequestHaveApple.SetActive(true);
        }
    }
}
