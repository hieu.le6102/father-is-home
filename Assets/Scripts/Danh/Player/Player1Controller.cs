﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player1Controller : MonoBehaviour
{
    public WinUI winUI;
    public Rigidbody2D body;
    public Transform tf;
    public HudController hud;
    // Start is called before the first frame update
    void Start()
    {
        body.gravityScale = 0;
    }

    // Update is called once per frame
    void Update()
    {

        if (Input.GetKey(KeyCode.W))
        {
            body.AddForce(Vector2.up * hud.speedSlice * hud.speedSlice * hud.speedSlice);
        }
        else if (Input.GetKey(KeyCode.S))
        {
            body.AddForce(Vector2.down * hud.speedSlice * hud.speedSlice * hud.speedSlice);
        }
    }

    public void KeyCodeW()
    {
        body.AddForce(Vector2.up * hud.speedSlice * hud.speedSlice * hud.speedSlice);
    }

    public void KeyCodeS()
    {
        body.AddForce(Vector2.down * hud.speedSlice * hud.speedSlice * hud.speedSlice);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        winUI.NotifiWin("Green");
    }

}
