﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : MonoBehaviour
{
    public Rigidbody2D body;
    public HudController hud;
    // Start is called before the first frame update
    void Start()
    {
        body.gravityScale = 0;
        GameObject gameObject = GameObject.Find("HUD");
        hud = gameObject.GetComponent<HudController>();

    }

    // Update is called once per frame
    void Update()
    {
        body.AddForce(Vector2.left * hud.speedEnemy);
    }

}
