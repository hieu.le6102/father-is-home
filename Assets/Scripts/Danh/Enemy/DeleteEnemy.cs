﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeleteEnemy : MonoBehaviour
{
    // Start is called before the first frame update
    public EnemyManager enemyManager;
    public float width;
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "EnemyUp")
        {
            enemyManager.DeleteEnemy();
            enemyManager.SpawnEnemy(Random.Range(0, enemyManager.enemyPrefabs.Length), 1);
            
        }
        if (collision.gameObject.tag == "EnemyDown")
        {
            enemyManager.DeleteEnemy();
            enemyManager.SpawnEnemy(Random.Range(0, enemyManager.enemyPrefabs.Length), 0);
        }
        
    }
    
}
