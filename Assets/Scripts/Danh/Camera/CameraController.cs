﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    public Rigidbody2D body;
    public float speed;
    // Start is called before the first frame update
    void Start()
    {
        body.gravityScale = 0;
        speed = 0f;
    }

    // Update is called once per frame
    void Update()
    {
        body.AddForce(Vector2.right * speed);
    }
}
