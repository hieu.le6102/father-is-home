﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RequestManager : MonoBehaviour
{
    // Start is called before the first frame update
    public Transform[] spawnPoint;
    public GameObject[] requestPrefabs;
    public GameObject activeRequest;
    public int currentPoint;
    void Start()
    {
        int requestIndex =  Random.Range(0, spawnPoint.Length);
        SpawnRequest(requestIndex);
    }
    void Update()
    {

    }
   
    public void SpawnRequest(int requestIndex)
    {
        GameObject request = (GameObject)Instantiate(requestPrefabs[requestIndex], spawnPoint[requestIndex].position, spawnPoint[requestIndex].rotation); 
        request.transform.position = new Vector3(request.transform.position.x, request.transform.position.y, 0);
        activeRequest = request;
    }

    public void DeleteRequest()
    {
        Destroy(activeRequest);
        int requestIndex =  Random.Range(0, spawnPoint.Length);
        SpawnRequest(requestIndex);
    }

}
