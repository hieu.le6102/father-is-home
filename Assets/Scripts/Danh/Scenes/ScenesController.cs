﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ScenesController : MonoBehaviour
{
    // Start is called before the first frame update
    public LoadScenes loadScenes;
    public string[] scenes;
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void ChangeScenes(string scenename)
    {
        Debug.Log("sceneName to load: " + scenename);
        var parameters = new LoadSceneParameters(LoadSceneMode.Single);
        SceneManager.LoadScene(scenename, parameters);
    }

    public static class StaticClass 
    {
        public static string CrossSceneInformation { get; set; }
    }  
}
