﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StoringSceneData : MonoBehaviour
{
    // Start is called before the first frame update
    public Transform playerTransform;
    public Image timerBar;
    
    void Start()
    {
        
    }

    void onDestroy(){
        PlayerPrefs.SetFloat("player_x", playerTransform.position.x);
        PlayerPrefs.SetFloat("player_y", playerTransform.position.y);
        PlayerPrefs.SetFloat("Time_left", timerBar.fillAmount * 5f);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
