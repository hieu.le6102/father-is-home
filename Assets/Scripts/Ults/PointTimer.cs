﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class PointTimer : MonoBehaviour
{
    // Start is called before the first frame update

    public Text scoreText;
    public float scoreAmount;
    public float pointIncreasse;
    void Start()
    {
        GameObject gameObject = GameObject.Find("SceneVariables");
        StoreVariable store = gameObject.GetComponent<StoreVariable>();
        scoreAmount = store.score;
        pointIncreasse = 1f;
    }

    // Update is called once per frame
    void Update()
    {
        scoreText.text = "Score: " + (int)scoreAmount;
        
    }
}
