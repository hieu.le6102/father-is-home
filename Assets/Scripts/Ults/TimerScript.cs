using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TimerScript : MonoBehaviour
{
    // Start is called before the first frame update

    Image timerBar;
    public float maxTime = 5f;
    float timeLeft;
    public GameObject gameOverPanel;  
    public Text yourScore;
    public Text currentScore;

    void Start()
    {
        if(Time.timeScale == 0){
            Time.timeScale = 1f;
        }
        gameOverPanel.SetActive(false);
        timerBar = GetComponent<Image>();
        timeLeft = maxTime;
        
    }

    // Update is called once per frame
    void Update()
    {        
        if(timeLeft > 0){
            timeLeft -= Time.deltaTime;
            timerBar.fillAmount = timeLeft / maxTime;
        }
        else{
            gameOverPanel.SetActive(true);
            yourScore.text = "Your" + currentScore.text;
            Time.timeScale = 0;
        }
    }
}
