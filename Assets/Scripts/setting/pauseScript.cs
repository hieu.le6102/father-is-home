﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class pauseScript : MonoBehaviour
{
    // Start is called before the first frame update


    public GameObject pauseMenu;
    public Button pauseBtn;

    public static bool paused = false;

    void Update(){

    } 

    public void resume(){
        pauseMenu.SetActive(false);
        Time.timeScale = 1f;
        paused = false;
    }

    public void pause(){
        if(paused == false){
            pauseMenu.SetActive(true);
            Time.timeScale = 0f;
            paused = true;
        }
    }
}
