﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TargetIndicator : MonoBehaviour
{
    public Transform Target;
    // Start is called before the first frame update
    public GameObject gameObject;
    public GameObject gameObject1;
    public GameObject gameObject2;
    public GameObject gameObject3;
    void Start()
    {
        

    }

    // Update is called once per frame
    void Update()
    {
        gameObject = GameObject.Find("QuestGarbageCollection(Clone)");
        gameObject1 = GameObject.Find("QuestDragAndDrop(Clone)");
        gameObject2 = GameObject.Find("QuestHaveApple(Clone)");
        gameObject3 = GameObject.Find("QuestDishWashing(Clone)");

        if (gameObject != null)
        {
            Target = gameObject.transform;
        }
        else if (gameObject1 != null)
        {
            Target = gameObject1.transform;
        }
        else if(gameObject2 != null)
        {
            Target = gameObject2.transform;
        }
        else if(gameObject3 != null)
        {
            Target = gameObject3.transform;
        }

        var dir = Target.position - transform.position;
        var angle = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg;
        transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);
        
    }
}
