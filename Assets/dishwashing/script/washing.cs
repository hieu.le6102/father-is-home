﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
public class washing : MonoBehaviour
{
    public Button[] dirt = new Button[11];
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        for (int i =0; i<11;i++)
        {
            if (dirt[i].IsActive())
            {
                return;
            }
        }
        var parameters = new LoadSceneParameters(LoadSceneMode.Single);
        GameObject gameObject = GameObject.Find("SceneVariables");
        StoreVariable store = gameObject.GetComponent<StoreVariable>();
        store.score += 1;
        SceneManager.LoadScene("Main Board", parameters);

    }
}
