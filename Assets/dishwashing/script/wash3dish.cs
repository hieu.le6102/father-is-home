﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class wash3dish : MonoBehaviour
{
    public Button[] dirt = new Button[11];
    public GameObject currentdish,nextdish;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        for (int i = 0; i < 11; i++)
        {
            if (dirt[i].IsActive())
            {
                return;
            }
        }
        currentdish.SetActive(false);
        nextdish.SetActive(true);

    }
}
