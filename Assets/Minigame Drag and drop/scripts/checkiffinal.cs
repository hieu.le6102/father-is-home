﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
public class checkiffinal : MonoBehaviour
{
    public Image[] item;
    public Image itemslot;
    private RectTransform[] rectTransform_item = new RectTransform[10];
    private RectTransform rectTransform_itemslot;
    // Start is called before the first frame update
    void Start()
    {
        
        rectTransform_itemslot  = itemslot.GetComponent<RectTransform>();
    }

    // Update is called once per frame
    void Update()
    {
        for (int i = 0; i < 10; i++)
        {
            rectTransform_item[i] = item[i].GetComponent<RectTransform>();
            if (rectTransform_item[i].anchoredPosition != rectTransform_itemslot.anchoredPosition)
            {
                return;
            }
        }
        var parameters = new LoadSceneParameters(LoadSceneMode.Single);
        GameObject gameObject = GameObject.Find("SceneVariables");
        StoreVariable store = gameObject.GetComponent<StoreVariable>();
        store.score += 1;
        SceneManager.LoadScene("Main Board", parameters);
        
    }
}
